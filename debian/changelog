libcss-squish-perl (0.10-2) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 11 Dec 2019 17:38:38 +0000

libcss-squish-perl (0.10-1) unstable; urgency=medium

  [ Jonathan Yu ]
  * New upstream release
  * Use new 3.0 (quilt) source format
  * Standards-Version 3.9.1 (no changes)

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/copyright: update wording of Comment about copyright
    ownership.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sat, 31 Mar 2018 14:12:52 +0300

libcss-squish-perl (0.09-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Fixes bug: "Modification of read-only value attempted
      at lib/CSS/Squish.pm line 220"
  * Standards-Version 3.8.3 (drop perl version dep)
  * Added myself to Uploaders and Copyright
  * Use short debhelper 7 rules format
  * Update to DEP5 copyright format
  * Rewrite control description

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update ryan52's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 14 Jan 2010 22:48:13 -0500

libcss-squish-perl (0.08-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Ryan Niebur ]
  * New upstream release
  * Add myself to Uploaders
  * Debian Policy 3.8.1
  * debhelper 7
  * machine readable copyright format

  [ gregor herrmann ]
  * debian/copyright: list all contributors for debian/*.
  * debian/control: make short description a noun phrase and extend long
    description.

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 24 Apr 2009 19:31:38 -0700

libcss-squish-perl (0.07-1) unstable; urgency=low

  * Initial Release. (Closes: #462142)

 -- Niko Tyni <ntyni@debian.org>  Wed, 23 Jan 2008 15:02:59 +0200
